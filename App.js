const instructionContainer = document.getElementById('instructions-container');
const newGameMenu = document.getElementById('new-game-menu');
const instructionOption = document.getElementById('instruction-option');
const newGameOption = document.getElementById('new-game');
const goBackToMainMenu = document.querySelectorAll('.go-back-main-menu');
const easyOptionButton = document.getElementById('easy');
const mediumOptionButton = document.getElementById('medium');
const hardOptionButton = document.getElementById('hard');
const gamePlayerArea = document.querySelector('.gameboard-play-area');
const gameBoardHeader = document.querySelector('.gameboard-header');
const gameBoard = document.getElementById('gameboard');
const homeFromGame = document.getElementById('home-from-game');
// const restartGame = document.getElementById('restart-game');
const popUpDisplay = document.getElementById('pop-up-display');
const timerClock = document.getElementById('time');

let totalClicks = 0;
let totalCardsToMatch;
let totalMatches = 0;
let timer;

instructionOption.addEventListener('click', () => {
  const instructionPage = document.getElementById('instruction-page');
  hideInstructionsContainer();
  instructionPage.style.display = 'flex';
});

homeFromGame.addEventListener('click', () => {
  clearInterval(timer);
  totalClicks = 0;
  totalMatches = 0;
  timerClock.innerText = '0:00'
  gameBoard.style.display = 'none';
  popUpDisplay.style.display = 'flex';

});

easyOptionButton.addEventListener('click', () => {
  const level = 4;
  hideNewGameMenu();
  showGame(level);
});

mediumOptionButton.addEventListener('click', () => {
  const level = 6;
  hideNewGameMenu();
  showGame(level);
});

hardOptionButton.addEventListener('click', () => {
  const level = 8;
  hideNewGameMenu();
  showGame(level);
});

newGameOption.addEventListener('click', () => {
  openNewGameMenu();
});

goBackToMainMenu.forEach((eachBackToMainButton) => {
  eachBackToMainButton.addEventListener('click', (event) => {
    const presentPage = event.target.parentNode.parentNode.parentNode.dataset.menu;
    showMainMenu(presentPage);
  });
});

const hideInstructionsContainer = () => {
  instructionContainer.style.display = 'none';
}

const showInstructionsContainer = () => {
  instructionContainer.style.display = 'flex';
}

const showNewGameMenu = () => {
  newGameMenu.style.display = 'flex';
}

const hideNewGameMenu = () => {
  newGameMenu.style.display = 'none';
}

const openNewGameMenu = () => {
  hideInstructionsContainer();
  showNewGameMenu();
}

const showMainMenu = (fromPage) => {
  if (fromPage === 'instruction') {
    document.getElementById('instruction-page').style.display = 'none';
  } else {
    hideNewGameMenu();
  }
  showInstructionsContainer();
}

const gifsCollection = new Array(37).fill(0).map((value, index) => {
  return `${index + 1}.gif`;
});

function showGame(level = 4) {
  const shuffledGifs = shuffle(gifsCollection);
  const gifImagesNeeded = shuffledGifs.slice(0, Math.floor((level * level) / 2));
  totalCardsToMatch = gifImagesNeeded.length;
  const shuffledPairedGifs = shuffle([...gifImagesNeeded, ...gifImagesNeeded]);
  createDivsForGifs(shuffledPairedGifs);

  if (level === 4) {
    document.querySelectorAll('#game .flip').forEach((cardNode) => {
      cardNode.style['width'] = `clamp(7.5rem,18vmin,15rem)`;
      cardNode.style['height'] = `clamp(7.5rem,18vmin,15rem)`;
    });
    gamePlayerArea.style['width'] = `clamp(${(7.5 * 5)}rem,${(18 * 5)}vmin,${(15 * 5)}rem)`;
    gamePlayerArea.style.display = 'flex';
    gameBoardHeader.style['width'] = `clamp(${(7.5 * 5)}rem,${(18 * 5)}vmin,${(15 * 5)}rem)`;
  } else if (level === 6) {
    document.querySelectorAll('#game .flip').forEach((cardNode) => {
      cardNode.style['width'] = `clamp(5.2rem,14vmin,15rem)`;
      cardNode.style['height'] = `clamp(5.2rem,14vmin,15rem)`;
    });
    gamePlayerArea.style['width'] = `clamp(${(5.2 * 7)}rem,${(14 * 7)}vmin,${(15 * 7)}rem)`;
    gamePlayerArea.style.display = 'flex';
    gameBoardHeader.style['width'] = `clamp(${(5.2 * 7)}rem,${(14 * 7)}vmin,${(15 * 7)}rem)`;
  } else {
    document.querySelectorAll('#game .flip').forEach((cardNode) => {
      cardNode.style['width'] = `clamp(4.1rem,9vmin,15rem)`;
      cardNode.style['height'] = `clamp(4.1rem,9vmin,15rem)`;
    });
    gamePlayerArea.style['width'] = `clamp(${(4.1 * 9)}rem,${(9 * 9)}vmin,${(15 * 9)}rem)`;
    gamePlayerArea.style.display = 'flex';
    gameBoardHeader.style['width'] = `clamp(${(4.1 * 9)}rem,${(9 * 9)}vmin,${(15 * 9)}rem)`;
  }
  gameBoard.style.display = 'flex';

}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


function createDivsForGifs(gifs) {
  for (let gif of gifs) {
    const newDiv = document.createElement("div");
    newDiv.dataset.gif = `${gif}`;

    newDiv.classList.add('flip');

    const frontSide = document.createElement('div');
    frontSide.classList.add('card-front');
    const img = document.createElement('img');
    img.src = `images/question_mark.png`;
    img.classList.add('card-face');

    frontSide.appendChild(img);

    const backSide = document.createElement('div');
    backSide.classList.add('card-back');
    backSide.style.background = `url(images/${gif})`;

    newDiv.appendChild(frontSide);
    newDiv.appendChild(backSide);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick, { capture: true });

    // append the div to the element with an id of game
    gamePlayerArea.append(newDiv);
  }
}

let numOfClicks = 0;
let previousCard = null;

// TODO: Implement this function!
function handleCardClick(event) {
  event.stopPropagation();
  const currentCard = event.target.parentNode;

  if (previousCard !== currentCard) {
    numOfClicks++;
    totalClicks++;

    currentCard.classList.add('open');
    currentCard.style.pointerEvents = 'none';
  }

  totalClicks === 1 ? gameTimer() : '';
  if (numOfClicks === 1) {
    previousCard = currentCard;
    return;
  }

  if (numOfClicks === 2) {
    document.querySelectorAll('#game .flip').forEach((card) => {
      card.style.pointerEvents = 'none';
    });

    if (previousCard.dataset.gif === currentCard.dataset.gif) {

      previousCard.classList.add('rubberBand');
      currentCard.classList.add('rubberBand');

      previousCard.classList.add('matched');
      currentCard.classList.add('matched');

      previousCard.firstChild.nextSibling.style['border'] = '3px solid green';
      event.target.style['border'] = '3px solid green';

      totalMatches++;

      if (totalCardsToMatch === totalMatches) {
        gameWon();
      }

    } else {

      previousCard.classList.add('wobble');
      currentCard.classList.add('wobble');

      previousCard.firstChild.nextSibling.style['border'] = '3px solid red';
      event.target.style['border'] = '3px solid red';

    }

    (function (firstCard, secondCard) {
      setTimeout(() => {

        if (firstCard.classList.contains('wobble')) {
          firstCard.classList.remove('wobble');
          secondCard.classList.remove('wobble');
        }

        if (firstCard.classList.contains('rubberBand')) {
          firstCard.classList.remove('rubberBand');
          secondCard.classList.remove('rubberBand');
        }

        firstCard.firstChild.nextSibling.style['border'] = 'none';
        secondCard.firstChild.nextSibling.style['border'] = 'none';

        document.querySelectorAll('#game .flip').forEach((card) => {
          if (!card.classList.contains('matched')) {
            card.style.pointerEvents = 'auto';
            card.classList.remove('open');
          }
        });
      }, 1000);
    })(previousCard, currentCard);

    numOfClicks = 0;
    previousCard = null;
    return;
  }
}


function gameTimer() {
  let startTime = new Date().getTime();

  // Update the timer every second
  timer = setInterval(() => {

    let now = new Date().getTime();

    // Find the time elapsed between now and start
    let elapsed = now - startTime;

    // Calculate minutes and seconds
    let minutes = Math.floor((elapsed % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((elapsed % (1000 * 60)) / 1000);

    if (seconds < 10) {
      seconds = "0" + seconds;
    }

    let currentTime = minutes + ':' + seconds;

    timerClock.innerText = `${currentTime}`;
  }, 750);
}


function gameWon() {
  clearInterval(timer);
  totalClicks = 0;
  totalMatches = 0;
  document.querySelector('.pop-up-content .confirmation-buttons').innerHTML = '';
  document.querySelector('.pop-up-content h2').innerText = 'You Won!';
  document.querySelector('.pop-up-content p').innerText = 'Congratulations';
  const button = document.createElement('button');
  button.classList.add('pushable');
  button.addEventListener('click', () => {
    displayMenu();
  });
  const spanInsideButton = document.createElement('span');
  spanInsideButton.classList.add('front');
  spanInsideButton.innerText = 'Go to Main Menu';
  button.appendChild(spanInsideButton);
  document.querySelector('.pop-up-content .confirmation-buttons').appendChild(button);

  gameBoard.style.display = 'none';
  popUpDisplay.style.display = 'flex';
}

function displayMenu() {
  document.getElementById('pop-up-display').style.display = 'none';
  document.getElementById('game').innerHTML = '';
  document.getElementById('instructions-container').style.display = 'flex';
}

function hidePopup() {
  document.getElementById('pop-up-display').style.display = 'none';
  document.getElementById('gameboard').style.display = 'flex';
}
